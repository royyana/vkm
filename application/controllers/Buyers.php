<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buyers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('courses_model');
	}

	public function myoutput($output = null)
	{
		$this->load->view('output',$output);
	}


	public function index()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');	
			$crud->set_table('buyer');
			$crud->set_subject('Buyer');
			$crud->unset_edit_fields('courses');
			$crud->set_relation_n_n('courses','buyer_course','course','buyer_id','course_id','[{course_code}] - {course_name} ({course_level_start}-{course_level_end})','priority');
			$crud->columns('buyer_id','buyer_name','buyer_budget','buyer_current_level','buyer_target_level');
			$crud->callback_column('coursetaken',array($this,'_buyer_course_taken'));
			$crud->unset_read();
			$crud->unset_delete();
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();
			$this->myoutput($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function _buyer_course_taken($value,$row){
		return "<a href='" . base_url('/buyers/courses/edit/' . $row->buyer_id) . "'>Course Taken</a>";
	}


	public function courses(){
		try{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('buyer');
			$crud->set_subject('Buyer');
			$crud->columns('buyer_id','buyer_name','buyer_budget','buyer_current_level','buyer_target_level','course_sequence_validity','buyer_budget','coursebudget','transaction');
			$crud->set_relation_n_n('courses','buyer_course','course','buyer_id','course_id','[{course_code}] - {course_name} ({course_level_start}-{course_level_end})','priority');
			$crud->callback_column('course_sequence_validity',array($this,'_buyer_course_sequence_validity'));
			$crud->callback_column('coursebudget',array($this,'_buyer_course_budget'));
			$crud->callback_column('transaction',array($this,'_buyer_transaction'));
			$crud->field_type('buyer_budget', 'invisible');
			$crud->field_type('buyer_current_level', 'invisible');
			$crud->field_type('buyer_target_level', 'invisible');

			$crud->unset_read();
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_export();
			$crud->unset_print();
			$output = $crud->render();
			$this->myoutput($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function _buyer_course_sequence_validity($value,$row){
		$buyer_data = $row;
		return $this->courses_model->is_buyer_course_valid($buyer_data);
	}

	public function _buyer_course_budget($value,$row){
		$buyer_data = $row;
		return $this->courses_model->get_buyer_course_price($buyer_data);
	}


	public function _buyer_transaction($value,$row){
		$buyer_data = $row;
		$mystring = ($this->courses_model->is_buyer_course_valid($buyer_data)=='VALID') ? '<a href="' . base_url('/buyers/transaction/' . $buyer_data->buyer_id ) . '">Transaction</a>' :'';
		return $mystring;
	}


	public function transaction($buyer_id=''){
		$data = array();
		$data['buyer_id']=$buyer_id;
		$data['tipe']='buyer';
		$data['info']=$this->courses_model->get_buyer_info($buyer_id);
		$data['nama']='buyer_name';
		$data['course_list']=$this->courses_model->get_buyer_courses($buyer_id);
		$this->load->view('transaction',$data);
	}


	public function buyer_accept($buyer_course_id='',$round=0,$url){
		$buyer_course_info=$this->courses_model->get_buyer_course_info($buyer_course_id);
		$seller_price = $buyer_course_info[0]['seller_price'];
		$this->courses_model->accept($buyer_course_id,$tipe='buyer',$round,$seller_price);
		redirect(base64_decode($url));
	}

	public function buyer_offer($buyer_course_id='',$round=0, $url){
		$offer_price = $this->input->post('offer_price');
		if ($offer_price==''){
			$data=array();
			$buyer_course_info=$this->courses_model->get_buyer_course_info($buyer_course_id);
			$data['price']=$buyer_course_info[0]['buyer_price'];
			$data['buyer_course_id']=$buyer_course_id;
			$data['round']=$round+1;
			$this->load->view('offerview',$data);
			return;
		}
		$this->courses_model->offer($buyer_course_id,$tipe='buyer',$round,$price=$offer_price);
		redirect(base64_decode($url));

	}

}