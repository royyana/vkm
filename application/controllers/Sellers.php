<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sellers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('courses_model');
	}

	public function myoutput($output = null)
	{
		$this->load->view('output',$output);
	}


	public function index()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('seller');
			$crud->set_subject('Seller');
			$crud->columns('seller_id','seller_name','courses');


			$crud->callback_column('courses',array($this,'_seller_courses'));

			$output = $crud->render();
			$this->myoutput($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function _seller_courses($value,$row){
		return "<a href='" . base_url('/sellers/courses/' . $row->seller_id) . "'>Courses</a>";
	}
	



	public function courses($seller_id='')
	{
		try{
			$crud = new grocery_CRUD();
			$crud->where('course_seller',$seller_id);
			$crud->set_theme('datatables');
			$crud->set_table('course');
			$crud->set_subject('Courses');
			$crud->columns('course_id','course_code','course_name','course_level_start','course_level_end','course_initial_price','courses_transaction');
			$crud->callback_column('courses_transaction',array($this,'_course_transaction'));
			$output = $crud->render();
			$this->myoutput($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function _course_transaction($value,$row){
		$seller_data = $row;
		$mystring = '<a href="' . base_url('/sellers/transaction/' . $seller_data->course_id ) . '">Transaction</a>';
		return $mystring;
	}



	public function transaction($course_id=''){
		$data = array();
		$data['course_id']=$course_id;
		$data['tipe']='seller';
		$data['info']=$this->courses_model->get_course_info($course_id);
		$data['nama']='course_name';
		$data['course_list']=$this->courses_model->get_courses_buyer($course_id);
		$this->load->view('transaction',$data);
	}

	public function seller_accept($buyer_course_id='',$round=0,$url){
		$buyer_course_info=$this->courses_model->get_buyer_course_info($buyer_course_id);
		$buyer_price = $buyer_course_info[0]['buyer_price'];
		$this->courses_model->accept($buyer_course_id,$tipe='seller',$round,$buyer_price);
		redirect(base64_decode($url));
	}



	public function seller_offer($buyer_course_id='',$round=0, $url){
		$offer_price = $this->input->post('offer_price');
		if ($offer_price==''){
			$data=array();
			$buyer_course_info=$this->courses_model->get_buyer_course_info($buyer_course_id);
			print_r($buyer_course_info);
			$data['price']=$buyer_course_info[0]['seller_price'];
			$data['buyer_course_id']=$buyer_course_id;
			$data['round']=$round+1;
			$this->load->view('offerview',$data);
			return;
		}
		$this->courses_model->offer($buyer_course_id,$tipe='seller',$round,$price=$offer_price);
		redirect(base64_decode($url));

	}


}
