<?php
class CourseGraph {
    function __construct(){
        $this->graph = array();
    }

    function add_edge($u,$v){
//        if (!in_array($v,array_values($this->graph[$u])))
            $is_in = isset($this->graph[$u]) ? in_array($v,array_values($this->graph[$u])) : false;
            if (!$is_in)
                $this->graph[$u][]=$v;

    }

    function get_graph(){
        return $this->graph;
    }


    function find_path($start,$end,$path=NULL){
        $this->start = $start;
        $this->end = $end;

        $_path = !is_null($path) ? $path : array();
        $_path = array_merge($_path,array($start));

        if ($this->start ==$this->end)
            return array($_path);

        if (!in_array($this->start,array_keys($this->graph)))
            return array();

        $paths = array();
        foreach($this->graph[$this->start] as $k=>$v){
            
            if (!in_array($v,array_values($_path))){
                $newpaths = $this->find_path($v, $this->end, $_path);
                foreach($newpaths  as $np){
                    $paths[]=$np;
                }
            }
        }
        return $paths; 
    }


}


class Courses_model  extends CI_Model  {


	function __construct()
    {
        parent::__construct();
    }


    function get_all_courses(){
        $query = $this->db->get('course');
        $b = $query->result_array();
        foreach($b as $k=>$v){
            $b[$k]['valid']=false;
        }
        return $b;
    }

    function get_buyer_courses($buyer_id=''){
        $this->db->query('update course C, buyer_course BC set BC.seller_price=C.course_initial_price where BC.bargain_round=1 and BC.seller_bargain_round=1 and C.course_id = BC.course_id');

        $this->db->select('*');
        $this->db->from('buyer_course');
        $this->db->where('buyer.buyer_id',$buyer_id);
        $this->db->join('course', 'course.course_id = buyer_course.course_id');
        $this->db->join('buyer', 'buyer.buyer_id = buyer_course.buyer_id');
        $this->db->order_by('priority','asc');
        $q = $this->db->get();
        $result = $q->result_array();
        return $result;
    }

    function get_courses_buyer($course_id=''){
        $this->db->query('update course C, buyer_course BC set BC.seller_price=C.course_initial_price where BC.bargain_round=1 and BC.seller_bargain_round=1 and C.course_id = BC.course_id');
        $this->db->select('*');
        $this->db->from('buyer_course');
        $this->db->where('course.course_id',$course_id);
        $this->db->join('course', 'course.course_id = buyer_course.course_id');
        $this->db->join('buyer', 'buyer.buyer_id = buyer_course.buyer_id');
        $this->db->order_by('priority','asc');
        $q = $this->db->get();
        $result = $q->result_array();
        return $result;
    }



    function is_buyer_course_valid($buyer_data){
        $buyer_id = $buyer_data->buyer_id;
        $buyer_current_level = $buyer_data->buyer_current_level;
        $buyer_target_level=$buyer_data->buyer_target_level;

        $courses = $this->get_buyer_courses($buyer_id);
        $kondisi=true;
        $kondisi1=isset($courses[0]['course_level_start']) ? ($buyer_current_level==$courses[0]['course_level_start']) : false;
        //echo 'KONDISI1'  . $buyer_id . '----' . $buyer_target_level . '>>>' . $courses[0]['course_level_start'] . '_' . $courses[0]['course_level_end'] . '-' . $kondisi1 . '<br>';

        $kondisi2=true;
        for($i=0;$i<count($courses)-1;$i++){
            $kondisi2=$kondisi2 & 
                (($courses[$i]['course_level_end']<=$courses[$i+1]['course_level_start']) || ($courses[$i+1]['course_level_end']>=$buyer_target_level));
            //echo 'KONDISI2'  . $courses[$i]['course_level_end'] . '_' . $courses[$i+1]['course_level_start'] . '-' . $kondisi2 . '<br>';
        }

        $kondisi3=false;
        for($i=0;$i<count($courses);$i++){
            $kondisi3=$kondisi3 | ($courses[$i]['course_level_end']>=($buyer_target_level)+0);
            //echo 'KONDISI3'  . $buyer_id . '----' . $buyer_target_level . '>>>' . $courses[$i]['course_level_start'] . '_' . $courses[$i]['course_level_end'] . '-' . $kondisi3 . '<br>';
        }

        $kondisi = $kondisi1 && $kondisi2 && $kondisi3;
        return ($kondisi==true) ? 'VALID' : 'NOT VALID';
    }


    function get_buyer_course_price($buyer_data){
        $buyer_id = $buyer_data->buyer_id;
        $price = 0;
        foreach($this->get_buyer_courses($buyer_id) as $k=>$v){
            $price+=$v['course_initial_price'];
        }
        return $price;
    }

    function get_course_info($course_id=''){
        $this->db->where('course_id',$course_id);
        $q=$this->db->get('course');
        return $q->result_array();
    }

    function get_buyer_info($buyer_id=''){
        $this->db->where('buyer_id',$buyer_id);
        $q=$this->db->get('buyer');
        return $q->result_array();
    }

    function get_buyer_course_info($buyer_course_id=''){
        $this->db->where('buyer_course_id',$buyer_course_id);
        $q=$this->db->get('buyer_course');
        return $q->result_array();
    }

    function get_seller_info($seller_id=''){
        $this->db->where('seller_id',$seller_id);
        $q=$this->db->get('seller');
        return $q->result_array();
    }

    function offer($buyer_course_id,$tipe='buyer',$round,$price=0){
        $data['bargain_round']=$round+1;
        $data[$tipe . '_price']=$price;
        $data[$tipe . '_bargain_round']=$round+1;
        $this->db->where('buyer_course_id',$buyer_course_id);
        $this->db->update('buyer_course',$data);
    }

    function accept($buyer_course_id,$tipe='buyer',$round,$price=0){
        $data['bargain_finish']=1;
        $data[$tipe . '_price']=$price;
        $this->db->where('buyer_course_id',$buyer_course_id);
        $this->db->update('buyer_course',$data);
    }


}