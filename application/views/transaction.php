<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="<?=base_url('/')?>/assets/bs/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="<?=base_url('/')?>/assets/bs/css/bootstrap-theme.min.css">
	<!-- Latest compiled and minified JavaScript -->
	<script src="h<?=base_url('/')?>/assets/bs/js/bootstrap.min.js"></script>
</head>
<?
if ($tipe=='buyer'){
	$actor='buyer';
	$id = 'buyer_id';
	$url1 =  	'/buyers/buyer_accept/';
	$url2 = 	'/buyers/buyer_offer/';

}

if ($tipe=='seller'){
	$actor = 'course';
	$id='course_id';
	$url1 =  	'/sellers/seller_accept/';
	$url2 = 	'/sellers/seller_offer/';

}



?>
<body style='margin:5px'>
	<h1><?=$info[0][$nama]?></h1>
	<a href='<?=base_url('/buyers/courses')?>' class='btn btn-primary'>BACK</a>
	<table  class="table table-bordered">
	<tr class='active'>
		<td>ID</td>
		<td>Buyer</td>
		<td>Course Title</td>
		<td>Level Begin</td>
		<td>Level End</td>
		<td>Price</td>
		<td>Bargain Round</td>
		<td>Buyer Bargain</td>
		<td>Seller Bargain</td>
		<td></td>
	</tr>

<?


$current_url = base64_encode(current_url());
foreach($course_list as $k=>$v){
	$made_bargain = ($v['bargain_round']==$v[$tipe . '_bargain_round']);
	$bargain_finish = ($v['bargain_finish']!='0');
?>
	<tr>
		<td><?=$v['buyer_course_id']?></td>
		<td><?=$v['buyer_name']?></td>
		<td><?=$v['course_name']?></td>
		<td><?=$v['course_level_start']?></td>
		<td><?=$v['course_level_end']?></td>
		<td><?=$v['course_initial_price']?></td>
		<td><?=$v['bargain_round']?></td>
		<td><?=$v['buyer_price']?></td>
		<td><?=$v['seller_price']?></td>
		<td>
		<?
			if ($bargain_finish){
?>
			<a class='btn'>BARGAIN HAS FINISHED</a>
<?
			} elseif (!$made_bargain) {

?>
			<a class='btn btn-primary' href='<?=base_url($url1 .  $v['buyer_course_id']. '/' . $v['bargain_round'] . '/' . $current_url)?>'>Accept Offer</a>&nbsp;
			<a class='btn btn-success' href='<?=base_url($url2 .  $v['buyer_course_id']. '/' .  $v['bargain_round'] . '/'  . $current_url)?>'>Bargain</a>&nbsp;
<?
			}			
			else {
?>
			<a class='btn btn-info'>BARGAIN HAS BEEN MADE</a>
<?				
			}

		?>
		</td>
	</tr>
<?	
}
?>	
	</table>

</body>
</html>