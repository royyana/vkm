<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="<?=base_url('/')?>/assets/bs/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="<?=base_url('/')?>/assets/bs/css/bootstrap-theme.min.css">
	<!-- Latest compiled and minified JavaScript -->
	<script src="h<?=base_url('/')?>/assets/bs/js/bootstrap.min.js"></script>
</head>
<body style='margin:5px'>
	<form method=post action="<?=base_url('/buyers/course_select')?>/<?=$buyer_id?>">
	<table  class="table table-bordered">
	<tr class='active'>
		<td>Select Course</td>
		<td>Course Title</td>
		<td>Level Begin</td>
		<td>Level End</td>
		<td>Price</td>
		<td>Seller</td>
	</tr>

<?

foreach($course_list as $k=>$v){
?>
	<tr>
		<td><input type=checkbox name='choose[]' value='<?=$v['course_id']?>'></td>
		<td><?=$v['course_name']?></td>
		<td><?=$v['course_level_start']?></td>
		<td><?=$v['course_level_end']?></td>
		<td><?=$v['course_initial_price']?></td>
		<td>By: <?=$v['course_seller']?></td>
	</tr>
<?	
}
?>	
	</table>
	<input type='submit' value='choose'>
	</form>
</body>
</html>