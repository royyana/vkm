-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: vkm
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `buyer`
--

DROP TABLE IF EXISTS `buyer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buyer` (
  `buyer_id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_name` varchar(100) DEFAULT NULL,
  `buyer_current_level` int(4) NOT NULL,
  `buyer_budget` decimal(7,0) NOT NULL DEFAULT '0',
  `buyer_target_level` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`buyer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buyer`
--

LOCK TABLES `buyer` WRITE;
/*!40000 ALTER TABLE `buyer` DISABLE KEYS */;
INSERT INTO `buyer` VALUES (1,'Phil Jones',1,1000,7),(2,'Van Aarnholt',1,1000,5),(3,'Daley Blind',1,800,10),(4,'Wim Jonk',1,500,10);
/*!40000 ALTER TABLE `buyer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buyer_course`
--

DROP TABLE IF EXISTS `buyer_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buyer_course` (
  `buyer_course_id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `bargain_round` int(11) NOT NULL DEFAULT '1',
  `seller_bargain_round` int(11) NOT NULL DEFAULT '1',
  `buyer_bargain_round` int(11) NOT NULL DEFAULT '0',
  `buyer_price` decimal(10,0) NOT NULL,
  `seller_price` decimal(10,0) NOT NULL,
  `bargain_finish` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`buyer_course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buyer_course`
--

LOCK TABLES `buyer_course` WRITE;
/*!40000 ALTER TABLE `buyer_course` DISABLE KEYS */;
INSERT INTO `buyer_course` VALUES (14,1,1,0,1,1,0,500,500,1),(15,1,3,1,1,1,0,400,400,1);
/*!40000 ALTER TABLE `buyer_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(500) DEFAULT NULL,
  `course_code` varchar(100) DEFAULT NULL,
  `course_level_start` int(2) NOT NULL DEFAULT '1',
  `course_level_end` int(2) NOT NULL DEFAULT '1',
  `course_initial_price` decimal(8,0) NOT NULL DEFAULT '0',
  `course_seller` int(11) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'ITS course 001','ITSX001',1,3,500,1),(2,'ITS 002','ITSX002',3,7,20,1),(3,'ITS X 003','ITSX003',1,10,400,1),(4,'ITS X 004','ITSX004',1,5,20,1),(5,'ITS X 005','ITSX005',5,10,200,1),(6,'ITS X 006','ITSX006',5,10,200,1),(7,'Univ of Indonesia 101','UI101',1,3,200,2),(8,'Univ of Indonesia 102','UI102',3,7,230,2),(9,'Univ of Indonesia 103','UI103',1,10,200,2),(10,'ITB 001','ITB001',1,5,10,3),(11,'ITB 002','ITB002',1,5,80,3);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller`
--

DROP TABLE IF EXISTS `seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller` (
  `seller_id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`seller_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller`
--

LOCK TABLES `seller` WRITE;
/*!40000 ALTER TABLE `seller` DISABLE KEYS */;
INSERT INTO `seller` VALUES (1,'ITS (Institut Teknologi Sepuluh Nopember)'),(2,'UI (University of Infonesia)'),(3,'ITB (Institut Teknologi Bandung)');
/*!40000 ALTER TABLE `seller` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-06  6:17:48
